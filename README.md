# Bad React Todo

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). It was intentionally created to include lots of bad code, so that a React developer can show their skills by changing this into good code.

Please spend some time to look through the code, mention what is bad about the code and why it is bad, and refactor until it's good. The clearer the explanation, and the cleaner to refactored code, the better it is.
