import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import TodoItem from './TodoItem';
import { useSelector,Provider, useDispatch } from 'react-redux'
import configureAppStore from './store'
import {addTodo} from './actions';
import { v4 as uuid } from 'uuid';

const store = configureAppStore();

function App() {
  return (
    <Provider store={store}>
    <div className="App">
      <h1>To Do App</h1>
      <p>This is a well coded ToDo App!</p>
      <TodoList />
      <AddTodos />
    </div>
    </Provider>
  );
}

const AddTodos = (props) => {
  const [value, setValue] = useState('');
  const dispatch = useDispatch();
  const todo = {
    index: uuid(),
    todo: value,
    completed: false,
  }
  const addItem = () => dispatch(addTodo(todo));
  return (
    <>
        <input type="text" value={value} onChange={(e) => setValue(e.target.value)} />
      <button onClick={addItem}>Add To Do</button>

    </>
  );
}

const TodoList = (props) => {
  const todos = useSelector((state) => state);
  let undone = []
  let done = []
  for (var i = 0; i < todos.length; i++) {
    if (todos[i].completed) {
      done.push(todos[i])
    } else {
      undone.push(todos[i])
    }
  }
  return (
    <div>
      <h2>Todos</h2>
    <ul>
        {undone.map((a, i) => <TodoItem {...a} />)}
      </ul>
    <h3>Done Items</h3>
    <ul>
        {done.map((a, i) => <TodoItem {...a} />)}
      </ul>
    </div>
  )
}

export default App;
