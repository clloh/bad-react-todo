import React from 'react';
import { connect } from 'react-redux';
import { removeTodo, toggleTodo } from './actions';

class TodoItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            index: props.index,
            todo: props.todo,
            done: props.completed,
        }
        this.action = this.action.bind(this);
        this.remove = this.remove.bind(this);
    }
    componentWillReceiveProps(props){
        if (props.index !== this.state.index || props.todo !== this.state.todo || props.completed !== this.state.done) {
            this.setState({
                index: props.index,
                todo: props.todo,
                done: props.completed,
            })
        }

    }

    action() {
        this.state.done = !this.state.done
        this.setState({todo: this.state.todo, done: this.state.done});
        this.props.markDone(this.state)
    }
    remove(todo) {
        this.props.remove(todo)
    }
    render() {
        const className = this.state.done ? 'strikethrough': '';
        return (<div className={className}><li>{this.state.todo}</li><button onClick={this.action}>Done</button>
        <button onClick={() => this.remove(this.state)}>Remove</button>
        </div>)
    }
}

const mapStateToProps = (state) => ({});
const mapDispatchToProps = (dispatch) => ({
    remove: (todo) => dispatch(removeTodo(todo)),
    markDone: (todo) => dispatch(toggleTodo(todo))
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoItem);
