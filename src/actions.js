import {ADD_TODO, TOGGLE_TODO, REMOVE_TODO} from './actionTypes';

export const addTodo = (todo) => ({type: ADD_TODO, payload: todo})
export const toggleTodo = (todo) => ({type: TOGGLE_TODO, payload: todo})
export const removeTodo = (todo) => ({type: REMOVE_TODO, payload: todo})
